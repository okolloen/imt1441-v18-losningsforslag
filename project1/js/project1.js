class WordCounter {
  constructor(input, output) {
    this.input=input;
    this.output=output;
  }

  count () {
    this.getWordsFromInput();
    this.countWords();
    this.sortWords();
    this.outputWords();
  }

  getWordsFromInput() {
    this.wordsList = document.querySelector(this.input).value.split(/[\n <>.,\?\(\)]/);
  }

  countWords() {
    this.words = [];
    this.count = [];
    for (var i = 0; i < this.wordsList.length; i++) {
      var index = this.words.indexOf(this.wordsList[i].toLowerCase());
      if (index==-1) {
        this.words.push(this.wordsList[i].toLowerCase());
        this.count.push(1);
      } else {
        this.count[index]++;
      }
    }
  }

  sortWords() {
    this.items = [];
    for (var i = 0; i < this.words.length; i++) {
      var item = {word: this.words[i], count: this.count[i]};
      this.items.push (item);
    }
    this.items.sort(function (a, b) {
      if (a.word>b.word)
        return 1;
      else if (a.word<b.word)
        return -1;
      else
        return 0;
    });
  }

  outputWords() {
    var tmpString = '';
    var letter = '';
    for (var i = 0; i < this.items.length; i++) {
      var firstLetter = this.items[i].word.substring(0,1);
      if (this.isLetter(firstLetter)) {
        if (letter!=firstLetter) {
          tmpString += '<h1>'+firstLetter+'</h1>';
          letter = firstLetter;
        }
        tmpString += this.items[i].word+" : "+this.items[i].count+'<br>';
      }
    }
    document.querySelector(this.output).innerHTML = tmpString;
  }

  isLetter(c) {
    return c.toLowerCase() != c.toUpperCase();
  }
}

var wordCounter = new WordCounter('#input', '#result');

document.querySelector("#knapp").addEventListener('click', e=>{
  wordCounter.count();
});
