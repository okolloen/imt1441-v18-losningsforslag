<?php
error_reporting(E_ERROR | E_PARSE);

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Content-Type: application/json; charset=utf-8");

$res = file_get_contents ('https://adressesok.posten.no/nb/addresses/search?utf8=✓&q='.urlencode($_GET['q']));

$doc = new DOMDocument();
@$doc->loadHTML('<?xml encoding="UTF-8">' . $res);

$xpath = new DOMXpath($doc);
$result = $xpath->query('//ul[@class="address_groups"]/li');
$addresses = [];
if ($result!=null) {
  foreach($result as $node) {
    $s = simplexml_import_dom($node);
    if (count(preg_split("/,\s/", $s->h3))<2)
      continue;
    [$gate, $tmp] = preg_split("/,\s/", $s->h3);
    [$postnr, $poststed] = explode(" ", $tmp);
    $nr = [];
    if (gettype($s->p->span) != 'undefined'&&$result->length<5) {
      foreach ($s->p->span as $address) {
        $nr[] = substr($address['aria-label'], strlen($gate)+1, strpos($address['aria-label'], '.', strlen($gate))-strlen($gate)-1);
      }
    } else {
      $nr[] = '';
    }
    $addr = array('gate'=>$gate, 'postnr'=>$postnr, 'sted'=>$poststed, 'nr'=>$nr);
    $addresses[] = $addr;
  }
} else {
  echo "not found";
}
while (count($addresses)>7) {
  $addresses = array_splice($addresses, 0, 7);
}

echo json_encode ($addresses, JSON_UNESCAPED_UNICODE);
