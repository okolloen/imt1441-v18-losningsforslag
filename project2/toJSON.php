<form method="post" action="toJSON.php">
  <textarea name="input"></textarea>
  <input type="submit" value="convert">
</form>

<?php

$data = [];
$lines = explode("\n", $_POST['input']);
for ($i = 0; $i<count($lines); $i++) {
  [$nr, $sted] = explode ("	", $lines[$i]);
  $data[] = array ('nr'=>$nr, 'sted'=>$sted);
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);
