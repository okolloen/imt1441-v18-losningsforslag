fetch('https://randomuser.me/api/?results=50')
.then(res=>res.json())
.then(json=> {
  json.results.forEach(user=>{
    var user = new User(user);
    document.querySelector('.userlist').innerHTML += user.toHTML();
  })
})

class User {
  constructor (user) {
    this.name = `${user.name.title} ${user.name.first} ${user.name.last}`;
    this.email = user.email;
    this.address = `${user.location.street}<br>${user.location.city}<br>${user.location.state} ${user.location.postcode}`;
    this.imageURL = user.picture.large;
  }

  toHTML() {
    return `<div class="user">
      <div class="img"><img src="${this.imageURL}"></div>
      <div class="nameLabel">Navn: </div><div class="name">${this.name}</div>
      <div class="emaillabel">Epost: </div><div class="email">${this.email}</div>
      <div class="addresslabel">Adresse: </div><div class="address">${this.address}</div>
    </div>`;
  }
}
