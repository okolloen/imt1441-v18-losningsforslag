// All koden i denne filen er hentet fra en forelesning i emnet (litt utvidet og tilpasset).
// Om de bruker denne eller en annen form for slideshow spiller ingen rolle
// så lenge det er en form for "animasjon" mellom bildene.

var images = ['images/plane1.jpg', 'images/plane2.jpg', 'images/plane3.jpg'];
var currentImage = 0;
var img = document.createElement('img');
var preloadImg = document.createElement('img');

window.addEventListener('load', e=>{        // When the document has loaded
  img.addEventListener('load', e=>{         // When the image has loaded
    document.querySelector('#slide1').style.backgroundImage = `url(${img.src})`;
    window.setTimeout (slideshow, 5000);    // Start the slideshow in 5 seconds
  })
  img.src = images[0];                      // Load first image in slideshow
})

/**
 * This method gets called when an image has been shown for x seconds.
 * The method starts the loading of the next image in the slideshow.
 */
function slideshow() {
  currentImage++;
  if (currentImage==images.length) {        // Reached the end so go back to the beginning
    currentImage = 0;
  }
  preloadImg.src = images[currentImage];
}

// When a new image has been loaded, it is time to fade the new image in (show the new image)
preloadImg.addEventListener('load', e=>{
  document.querySelector('#slide2').style.backgroundImage = `url(${preloadImg.src})`;
  document.querySelector('#slide2').style.opacity = 1;
})

// When the new image has been made fully visible, remove the old image and get ready
// to load a new image in x seconds time.
document.querySelector('#slide2').addEventListener('transitionend',e => {
  document.querySelector('#slide1').style.backgroundImage = document.querySelector('#slide2').style.backgroundImage;
  if (document.querySelector('#slide2').style.opacity == 1) {
    document.querySelector('#slide2').style.opacity = 0;
    setTimeout(slideshow, 5000);  // We are ready to start the process over.
  }
})
