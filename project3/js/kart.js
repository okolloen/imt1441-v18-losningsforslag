// Koden her er hentet fra forelesning, tilpasset til bruken her.

var mapElement = document.querySelector("main.map section");
var map;    // The map lives here

/**
 * Laster inn google map api'et. Må gjøres slik for å kunne legge til API nøkkelen
 * dynamisk (ikke hardkodet.)
 *
 * @return Promise et løfte som innfris når API'et er lastet inn.
 */
function loadGoogleMapAPI() {
  return new Promise((resolve, reject)=>{
    var script = document.createElement('script');    // Opprett en skript tag
    script.addEventListener('load', resolve);         // Kall resolve når det er ferdig lastet
    script.src = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}`;
    document.querySelector('head').appendChild(script);  // Legg til i head (dette trigger innlastingen)
  });
}

window.addEventListener('load', e=>{
  loadGoogleMapAPI().then (()=>{                // Når google map API'et er lastet inn
    map = new google.maps.Map(mapElement, {     // Opprett et kart, vis dette i mapElement
      center: {lat: 60.7900984, lng: 10.6843},  // Sentrer kartet rundt NTNU i Gjøvik
      zoom: 14                                  // 1: World, 5: Landmass/continent, 10: City, 15: Streets, 20: Buildings
    });
    map.addListener('click', e=>{               // Når brukeren klikker i kartet
      console.log (e.latLng.toJSON());          // Logg informasjon om plassering
      visInfoOmLatLng({lat: e.latLng.lat(), lng: e.latLng.lng()});  // >Vis informasjon om plassering
    })
  })
})

/**
 * Tar i mot informasjon om plassering (lat/lng) og viser informasjon om plasseringen.
 * Bruker google for å finne adressen til et gitt punkt i kartet.
 *
 * Informasjonen vises i HTML elementet med id=posInfo
 *
 * @param  Object  latLng et objekt med lat og lng for posisjonen vi ønsker å vise informasjon om.
 */
function visInfoOmLatLng(latLng) {
  // Hent informasjon om stedet fra google geocode api'et
  fetch (`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latLng.lat},${latLng.lng}&key=${API_KEY}`)
  .then(res => res.json())              // Dekoder json data
  .then(json=>{                         // json inneholder nå et objekter
    console.log (json);                 // Hva fikk vi egentlig, for debugging/utviklingsformål
    var html = JSON.stringify(latLng);  // Gjør om objektet vi fikk inn til en tekst, enkel måte å vise posisjonen på
    var i=0;                            // Ønsker primært å vise en gateadresse, så vi prøver å finne en
    while (json.results[i].types[0]!='street_address'&&i<json.results.length-1) {
      i++;
    }
    html += `<br>${json.results[i].formatted_address}`;   // Legger til den formaterte adressen i teksten
    document.querySelector('#address').innerHTML = html;  // Og viser posisjon og adresse i elementet med id=posInfo


    // Bruker streetview for å generere bilde fra riktig sted og i riktig retning
    document.querySelector('#north img').src = `https://maps.googleapis.com/maps/api/streetview?location=${latLng.lat},${latLng.lng}&size=300x250&heading=0&key=${API_KEY}&fov=180`;
    document.querySelector('#east img').src = `https://maps.googleapis.com/maps/api/streetview?location=${latLng.lat},${latLng.lng}&size=300x250&heading=90&key=${API_KEY}&fov=180`;
    document.querySelector('#south img').src = `https://maps.googleapis.com/maps/api/streetview?location=${latLng.lat},${latLng.lng}&size=300x250&heading=180&key=${API_KEY}&fov=180`;
    document.querySelector('#west img').src = `https://maps.googleapis.com/maps/api/streetview?location=${latLng.lat},${latLng.lng}&size=300x250&heading=270&key=${API_KEY}&fov=180`;

  })
}
