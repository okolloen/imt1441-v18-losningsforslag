document.querySelector('button').addEventListener('click', e=>{
  var method=document.querySelector('input[type="radio"]:checked');
  if (method!=null) {           // User has selected GET/POST
    if (method.value=='GET') {
      var searchParams = new URLSearchParams();
      searchParams.append ('navn', valueOfId('navn'));
      searchParams.append ('adresse', valueOfId('adresse'));
      searchParams.append ('postnr', valueOfId('postnr'));
      searchParams.append ('sted', valueOfId('sted'));
      fetch(`http://folk.ntnu.no/oeivindk/imt1441/echo.php?${searchParams.toString()}`)
      .then(res=>res.json())
      .then(json=> {
        document.querySelector('.output').innerHTML = JSON.stringify(json, null, 2);
      });
    } else {                  // POST selected
      var data = new FormData(document.querySelector('form'));
      fetch(`http://folk.ntnu.no/oeivindk/imt1441/echo.php`, {
        method: 'POST',
        body: data
      })
      .then(res=>res.json())
      .then(json=> {
        document.querySelector('.output').innerHTML = JSON.stringify(json, null, 2);
      });
    }
  }
})

/**
 * Return the value of the field with the given ID
 * @param  {String} id the id of the field to get the value from
 * @return {String}    the value of the field
 */
function valueOfId(id) {
  document.getElementById(id).value;
}
